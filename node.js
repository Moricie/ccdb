let express = require('express');
let bodyParser = require('body-parser');
let fs = require('fs');
let formidable = require('formidable');
let AWS = require('aws-sdk');

let s3 = new AWS.S3();
// let awsCli = require('aws-cli-js');
// let Options = awsCli.Options;
// let Aws = awsCli.Aws;

let app = express();
const bucketName = 'dydbucketforcloudcomputing0';

s3.listBuckets({}, (err, data) =>
{
  var foundBucket = false;
  if(err) console.log(err, err.stack);
  else 
  {
    buckets = data.Buckets;
    console.log(buckets);
    for(let i = 0; i < buckets.length; i++)
    {
      if(buckets[i].Name === bucketName)
      {
        console.log('Found bucket' + bucketName);
        foundBucket = true;
      }
    }
    if (foundBucket === false)
    {
      console.log('Did not find bucket' + bucketName);
      console.log('Creating bucket');
      
      s3.createBucket({Bucket: bucketName}, function(err, data) {

        if (err) {
        
           console.log(err);
        
           } else {
        
              console.log('Bucket ' + bucketName + ' created.')     
        
           }
        
        });
    }
  }
});



  

/*
let options = new Options(
     TEMP_ACCESS_ID,
     TEMP_SECRET_KEY,
     TEMP_TOKEN,
  );

  var aws = new Aws(options);

  aws.command('s3 ls', () => 
  {

  });
*/
app.use(express.static('./public'));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.post('/files', express.static('./public'), (req, res) => 
{

    var form = new formidable.IncomingForm();

    form.parse(req, function(err, fields, files) {
      res.writeHead(200, {'content-type': 'text/html'});

      // The next function call, and the require of 'fs' above, are the only
      // changes I made from the sample code on the formidable github
      // 
      // This simply reads the file from the tempfile path and echoes backre
      // the contents to the response.

      var name = fields.name;
      //console.log(name);

        fs.readFile(files.file.path, function (err, data) {
/*
            fs.writeFile(files.file.name, data, (err) => 
            {
              
            });
*/
            params = {Bucket: bucketName, Key: name + '/' + files.file.name, Body: data};
    
         s3.putObject(params, function(err, data) {
    
             if (err) {
    
                 console.log(err)
    
             } else {
    
              //   console.log("Successfully uploaded data to myBucket/myKey");
    
             }
    
          });

        res.end("<p>Your files are uploading! This can take a few seconds.</p>");
      });
    });

});

app.get('/files/down', express.static('./public'), (req, res) => 
{
  //var form = new formidable.IncomingForm();

    //form.parse(req, function(err, fields, files) {
      res.writeHead(200, {'content-type': 'text/html'});
  
      var params = 
      {
        Bucket: bucketName,
        MaxKeys: 1000
      };
      s3.listObjects(params, (err, data) =>
      {
        if (err)
        {
          console.log(err, err.stack);
        }
        else
        {
          let output = data.Contents;

          if(output.length == 0)
          {
            res.end('<h3>No files saved yet.</h3>');
          }
          else
          {
          let userOutput = [];
          for(let i = 0; i < output.length; i++)
          {
            if(output[i].Key.startsWith(req.query.name) && output[i].Key.substr(req.query.name.length, 1) == '/')
            {
              userOutput.push(output[i]);
            }
          }
          
          // console.log(output.length);
          // console.log(fields.name);
          res.write('<h3>Download</h3>');
          for(let i = 0; i < userOutput.length; i++)
            {
              let fileName = userOutput[i].Key.substr(req.query.name.length + 1, userOutput[i].Key.length - 1);
              var params = {Bucket: bucketName, Key: userOutput[i].Key};
              var url = s3.getSignedUrl('getObject', params);
              res.write(`<p><a href=${url}><button class="downloadButton">${fileName}</button></a> ${Math.round((((userOutput[i].Size / 1024) / 1024) * 100)) / 100}MB</p>`)
            };
          
            res.end();

          //console.log(data);
          }
        }
      });
    //});
  });

  app.get('/files/del', express.static('./public'), (req, res) =>
  {
    res.writeHead(200, {'content-type': 'text/html'});

      var params = 
      {
        Bucket: bucketName,
        MaxKeys: 1000
      };
      s3.listObjects(params, (err, data) =>
      {
        if (err)
        {
          console.log(err, err.stack);
        }
        else
        {
          let output = data.Contents;

          if(output.length == 0)
          {
            res.end('<h3>No files saved yet.</h3>');
          }
          else
          {
          let userOutput = [];
          for(let i = 0; i < output.length; i++)
          {
            if(output[i].Key.startsWith(req.query.name) && output[i].Key.substr(req.query.name.length, 1) == '/')
            {
              userOutput.push(output[i]);
            }
          }
          
          // console.log(output.length);
          // console.log(fields.name);
          res.write('<h3>Deletetion</h3>');

          for(let i = 0; i < userOutput.length; i++)
            {
              let fileName = userOutput[i].Key.substr(req.query.name.length + 1, userOutput[i].Key.length - 1);
              var params = {Bucket: bucketName, Key: userOutput[i].Key};
              var url = s3.getSignedUrl('getObject', params);
              res.write(`<p><button class="deleteButton" onclick=deleteFile('${fileName}')>${fileName}</button> ${Math.round((((userOutput[i].Size / 1024) / 1024) * 100)) / 100}MB</p>`)
            };
          
            res.end();

          //console.log(data);
        }
      }
    });
  });

app.delete('/files/:fId', express.static('./static'), (req, res) => 
{
  res.writeHead(200, {'content-type': 'text/html'});
 var params = {
  Bucket: bucketName, 
  Key: req.query.name + '/' + req.params.fId
 };

 s3.deleteObject(params, function(err, data) {
   if (err) console.log(err, err.stack); // an error occurred
   //else     console.log(data);           // successful response
   /*
   data = {
   }
   */
 });
});

app.get('/storage', express.static('./static'), (req, res) =>
{
  res.writeHead(200, {'content-type': 'text/html'});
  let name = req.query.name;
      var params = 
      {
        Bucket: bucketName,
        MaxKeys: 1000
      };
      s3.listObjects(params, (err, data) =>
      {
        if (err)
        {
          console.log(err, err.stack);
        }
        else
        {
          let output = data.Contents;
          //console.log(output);
          let userOutput = [];
          for(let i = 0; i < output.length; i++)
          {
            //console.log(output[i].Key.substr(req.query.name.length, 1));
            if(output[i].Key.startsWith(req.query.name) && output[i].Key.substr(req.query.name.length, 1) == '/')
            {
              //console.log(output[i].Key);
              userOutput.push(output[i]);
            }
          }
          let usedStorage = 0;
          for(let i = 0; i < userOutput.length; i++)
          {
            //console.log(userOutput[i].Size);
            usedStorage += userOutput[i].Size;
          }
          
          res.end(`${Math.round(((((usedStorage / 1024) / 1024)) * 100)) / 100}`);
        }
      });
});

app.listen(80);

console.log('...');
