
$(document).ready(main)
function main()
{
    
    let name = $('#name')[0].value;
    let refreshUrl = '/files/down';
        refreshUrl += `?name=${name}`;
        if(name !== '')
        {
            loadAjaxGet(refreshUrl);
            loadAjaxStorage();
        }
        
/*
    function loadF(url) {
        console.log("fetch")
        fetch(url)
            .then(function(response){
                console.log(response)})
    }

    function load(url){
        let x = new XMLHttpRequest();
        x.open("GET" ,url ,true);
        x.onreadystatechange=function(){
            if(this.readyState==4 && this.status==200){
                r = x.responseText;
                $("#ausgabe")[0].innerHTML = r;
            }
        };
        x.send();
    }
*/

function loadAjaxStorage(){
    $.ajax({
        url: `/storage?name=${$('#name')[0].value}`,
        type: "GET",
        success: function(result){
            $("#storausgabe")[0].innerHTML = `You are currently using ${result}MB`;
        },
        error:function(error){
             console.log(error);
        }
    })
}

    function loadAjaxGet(url){
        $.ajax({
            url: url,
            type: "GET",
            success: function(result){
                $("#ausgabe")[0].innerHTML = result;
            },
            error:function(error){
                console.log(error);
            }
        })
    }


    function loadAjaxPost(url, dat){
        $.ajax({
            url: url,
            type: "POST",
            data: dat,
            processData: false,
            contentType: false,
            success: function(result){
                $("#ausgabe")[0].innerHTML = result;
                console.log(result);
            },
            error:function(error){
                console.log(error);
            }
        })
    }

    function loadAjaxDelete(url){
        $.ajax({
            url: url,
            type: "DELETE",
            success: function(result){
                $("#ausgabe")[0].innerHTML = result;
            },
            error:function(error){
                console.log(error);
            }
        })
    }

    var fileInput = $('#file')[0];
    var fileList = [];

    fileInput.addEventListener('change', (event) => 
    {
        fileList = [];
        for (let i = 0; i < fileInput.files.length; i++)
        {
            fileList.push(fileInput.files[i]);
        }
        
    });

    document.getElementById("sendFiles").onclick = () =>
        
        {
            //let url = 'http://ccdb-1140143492.us-east-1.elb.amazonaws.com/upload';
            let url = '/files';
            let name = $('#name')[0].value;
            let formData = new FormData();
            formData.append("name", name);
            
            if(fileList.length == 0)
            {
                $('#ausgabe')[0].innerHTML = '<h3>Please select files to upload first.</h3>';
            }
            else if(name == '')
            {
                $('#ausgabe')[0].innerHTML = '<h3>Please enter a username.</h3>'
            }
            else
            {
                fileList.forEach((file) => 
                {
                    formData.append('file', file);
                    loadAjaxPost(url, formData);
                    formData.delete('file');
                });
                fileList = [];
                $('#file')[0].value = '';
                loadAjaxStorage();

                //formData.append("file", file);
                //loadAjaxPost(url, formData);
            }
        };


    document.getElementById("listFilesDownload").onclick = () =>
    {
        let url = '/files/down'
        let name = $('#name')[0].value;
        url += `?name=${name}`;
        loadAjaxGet(url);
        loadAjaxStorage();
    };

    document.getElementById("listFilesDeletion").onclick = () =>
    {
        let url = '/files/del';
        let name = $('#name')[0].value;
        url += `?name=${name}`;
        loadAjaxGet(url);
        loadAjaxStorage();
    };

    window.deleteFile = (key) =>
    {
        let url = `/files/${key}`;
        let name = $('#name')[0].value;
        url += `?name=${name}`;
        loadAjaxDelete(url);

        setTimeout(() => 
        {
            let refreshUrl = '/files/del';
            refreshUrl += `?name=${name}`;
            loadAjaxGet(refreshUrl);
            loadAjaxStorage();
        }, 1000);
    };

    function Sleep(milliseconds) {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
     }     

    async function deletionWait() {
        console.log("Vor der sleep-Funktion");
        await Sleep(1000); //milliseconds
        console.log("Nach der Sleep Funktion");
     }

/*
    window.download = function(key)
    {
        let name = $('#name')[0].value;
        console.log(key);
        let url = `/files/${key}`;
        url += `?name=${name}`;
        console.log(url);
        loadAjaxGet(url);
        
        let listUrl = '/files';
        listUrl += `?name=${name}`;
        console.log(listUrl);
        loadAjaxGet(listUrl);
    }
*/
}

